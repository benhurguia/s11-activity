package com.zuitt.activity.repositories;

import com.zuitt.activity.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//an interface marked as @Repository contains method for database manipulation
@Repository
//By extending Crud Repository  will inherit its predefine method for creating , retrieving ,updating and deleting records
public interface PostRepository extends CrudRepository<Post, Object> {
}
