package com.zuitt.activity.services;

import com.zuitt.activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {


    void createUser(User post);

    Iterable<User> getUsers();

    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id, User post);
}
